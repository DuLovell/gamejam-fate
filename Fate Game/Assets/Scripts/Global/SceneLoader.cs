using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private string _mainMenuSceneName;
    [SerializeField] private string _gameplaySceneName;

    public static SceneLoader Instance { get; private set; }


    private void Awake()
    {
        Instance = this;
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    public void LoadMainMenuScene()
    {
        SceneManager.LoadScene(_mainMenuSceneName);
    }

    public void LoadGameplayScene()
    {
        SceneManager.LoadScene(_gameplaySceneName);
    }
}
