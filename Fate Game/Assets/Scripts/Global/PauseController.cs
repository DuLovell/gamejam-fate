using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour
{
    private void Start()
    {
        MenuManager.Instance.OnMenuChanged += SelectGameState;
    }

    private void OnDisable()
    {
        if (MenuManager.Instance != null)
        {
            MenuManager.Instance.OnMenuChanged -= SelectGameState;
        }
    }

    private void SelectGameState(Menu _menu)
    {
        if (_menu.PauseGameWhenOpen == true)
        {
            PauseGame();
        }
        else
        {
            ResumeGame();
        }
    }

    private void PauseGame()
    {
        Time.timeScale = 0;
    }

    private void ResumeGame()
    {
        Time.timeScale = 1;
    }
}
