using System;
using UnityEngine;
using UnityEngine.Audio;

namespace Global
{
    public class AudioController : MonoBehaviour
    {
        [SerializeField] private AudioMixer _mainMixer;


        public static AudioController Instance { get; private set; }
        
        public AudioMixer MainMixer => _mainMixer;


        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        private void OnDestroy()
        {
            Instance = null;
        }
    }
}
