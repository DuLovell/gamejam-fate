﻿using System.Collections;
using Data.Level;
using Level;
using UI;
using UnityEngine;
using Environment = Level.Environment;

namespace Global
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private LevelData _data;
        [SerializeField] private Environment _environment;
        [SerializeField] private UIDiceRoller _diceRoller;

        private LevelInitializer _levelInitializer;
        
        private readonly WaitForSeconds _diceCanvasTurnOffDelay = new WaitForSeconds(2f);
    
        private void Awake()
        {
            _levelInitializer = new LevelInitializer(_data, _environment);
        }

        private void Start()
        {
            _diceRoller.DiceRolled += OnDiceRolled;
        }

        private void OnDiceRolled(int result)
        {
            StartCoroutine(StartLevelRoutine(result));
        }
    

        private IEnumerator StartLevelRoutine(int rollResult)
        {
            yield return TurnOffDiceCanvasRoutine();
            
            _levelInitializer.StartInitialization(rollResult);
        }

        private IEnumerator TurnOffDiceCanvasRoutine()
        {
            yield return _diceCanvasTurnOffDelay;
            _diceRoller.gameObject.SetActive(false);
        }
    }
}