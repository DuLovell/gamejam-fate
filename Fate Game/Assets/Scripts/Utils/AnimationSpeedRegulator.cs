using UnityEngine;

namespace Utils
{
    public static class AnimationSpeedRegulator
    {
        private static readonly int _speedMultiplier = Animator.StringToHash("speedMultiplier");
        
        public static void SetCurrentAnimationSpeed(Animator animator, float animationClipLength, float desiredLength = 0f)
        {
            float animationSpeedMultiplier = desiredLength == 0f ? 1f : animationClipLength / desiredLength;
            animator.SetFloat(_speedMultiplier, animationSpeedMultiplier);
        }
    }
}
