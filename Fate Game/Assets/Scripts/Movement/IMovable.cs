﻿namespace Movement
{
    public interface IMovable
    {
        public void Move(float speed);
    }
}