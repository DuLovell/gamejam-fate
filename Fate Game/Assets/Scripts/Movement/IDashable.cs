﻿namespace Movement
{
    public interface IDashable
    {
        public bool IsDashing { get; }
        
        public void Dash(float duration, float distance);
    }
}