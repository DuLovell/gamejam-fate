﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Movement
{
    public class PlayerDash : MonoBehaviour, IDashable, ICooldownable
    {
        private Rigidbody2D _rigidbody2D;
        private Vector2 _direction;
        
        public bool IsDashing { get; private set; }

        public float Cooldown { get; set; }
        public bool IsOnCooldown { get; private set; }
        
        public void OnMoveInput(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _direction = context.ReadValue<Vector2>();
            }
            else 
            {
                _direction = Vector2.zero;
            }
        }
        
        public void Dash(float duration, float distance)
        {
            Vector2 newPosition = _rigidbody2D.position + _direction * distance;
            StartCoroutine(DashRoutine(duration, newPosition));
        }

        private void Awake()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
        }

        private IEnumerator DashRoutine(float duration, Vector2 targetPosition)
        {
            IsDashing = true;

            Vector2 startPosition  = _rigidbody2D.position;
            float elapsedTime = 0;
         
            while (elapsedTime < duration)
            {
                _rigidbody2D.position = Vector2.Lerp(startPosition, targetPosition, (elapsedTime / duration));
                elapsedTime += Time.deltaTime;
                yield return null;
            }

            _rigidbody2D.position = targetPosition;
            
            IsDashing = false;
            
            StartCoroutine(CooldownRoutine());
        }

        private IEnumerator CooldownRoutine()
        {
            IsOnCooldown = true;
            yield return new WaitForSeconds(Cooldown);
            IsOnCooldown = false;
        }
    }
}