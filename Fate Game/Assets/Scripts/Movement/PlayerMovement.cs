﻿using System.Collections;
using Graphics;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Movement
{
    [RequireComponent(typeof(SpriteLookDirection))]
    public class PlayerMovement : MonoBehaviour, IMovable
    {
        private Rigidbody2D _rigidbody2D;
        private SpriteLookDirection _spriteLook;
        private Vector2 _direction;

        public bool IsDashing { get; private set; }
        public float Cooldown { get; private set; }
        public bool IsOnCooldown { get; private set; }

        public void OnMoveInput(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _direction = context.ReadValue<Vector2>();
            }
            else 
            {
                _direction = Vector2.zero;
            }
        }

        public void Move(float speed)
        {
            _rigidbody2D.MovePosition(Vector2.MoveTowards(_rigidbody2D.position, 
                _rigidbody2D.position + _direction, speed));
            _spriteLook.SetLookDirection(_direction);
        }

        /*public void Dash(float duration, float distance)
        {
            Vector2 newPosition = _rigidbody2D.position + _direction * distance;
            StartCoroutine(DashRoutine(duration, newPosition));
        }
        
        private IEnumerator DashRoutine(float duration, Vector2 targetPosition)
        {
            IsDashing = true;
            IsOnCooldown = true;
            
            Vector2 startPosition  = _rigidbody2D.position;
            float elapsedTime = 0;
         
            while (elapsedTime < duration)
            {
                _rigidbody2D.position = Vector2.Lerp(startPosition, targetPosition, (elapsedTime / duration));
                elapsedTime += Time.deltaTime;
                yield return null;
            }

            _rigidbody2D.position = targetPosition;
            
            IsDashing = false;

            yield return new WaitForSeconds(2f);
            IsOnCooldown = false;
        }*/

        private void Awake()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
            _spriteLook = GetComponent<SpriteLookDirection>();
        }
    }
}
