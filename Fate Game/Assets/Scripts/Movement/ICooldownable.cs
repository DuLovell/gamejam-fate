﻿namespace Movement
{
    public interface ICooldownable
    {
        public float Cooldown { get; set; }
        
        public bool IsOnCooldown { get; }
    }
}