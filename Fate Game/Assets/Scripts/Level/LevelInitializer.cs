﻿using System;
using System.Collections;
using Data.Level;
using UI;
using UnityEngine;

namespace Level
{
    public class LevelInitializer
    {
        private readonly LevelData _data;
        private readonly Environment _environment;

        private readonly Spawner _spawner;

        public LevelInitializer(LevelData data, Environment environment)
        {
            _data = data;
            _environment = environment;

            _spawner = new Spawner();
        }

        public void StartInitialization(int rollResult)
        {
            _data.DataToRandomize.RuntimeValue = rollResult;
            _environment.SetBackground(_data.EnvironmentTilemap);
            _spawner.SpawnAtRandomPositionInsideScreen(_data.Enemies);
        }
    }
}