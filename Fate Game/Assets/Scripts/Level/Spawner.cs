﻿using System.Collections.Generic;
using Data.Level;
using UnityEngine;

namespace Level
{
    public class Spawner
    {
        private readonly Camera _camera;
        
        public Spawner()
        {
            _camera = Camera.main;
        }

        //TODO Спавнить врагов так, чтобы они сразу смотрели на игрока
        public List<GameObject> SpawnAtRandomPositionInsideScreen(IEnumerable<EnemyToSpawn> enemies)
        {
            List<GameObject> spawnedEnemies = new List<GameObject>();
            foreach (EnemyToSpawn enemy in enemies)
            {
                for (int i = 0; i < enemy.Count; i++)
                {
                    Vector3 spawnPosition = GetRandomPositionInsideScreen();
                    GameObject spawnedEnemy = Object.Instantiate(enemy.EnemyTemplate, spawnPosition, Quaternion.identity);
                    spawnedEnemies.Add(spawnedEnemy);
                }
            }

            return spawnedEnemies;
        }

        public GameObject SpawnAtPosition(GameObject gameObject, Vector3 spawnPosition)
        {
            return Object.Instantiate(gameObject, spawnPosition, Quaternion.identity);
        }

        private Vector2 GetRandomPositionInsideScreen()
        {
            return _camera.ViewportToWorldPoint(new Vector2(Random.value, Random.value));
        }
    }
}