﻿using UnityEngine;
using UnityEngine.Tilemaps;

namespace Level
{
    [RequireComponent(typeof(Grid))]
    public class Environment : MonoBehaviour
    {
        public void SetBackground(Tilemap tilemap)
        {
            Instantiate(tilemap, transform);
        }
    }
}