﻿using UnityEngine;

namespace Graphics
{
    public class SpriteLookDirection : MonoBehaviour
    {
        [SerializeField] private bool _initialDirectionRight;
        
        private SpriteRenderer[] _sprites;

        private void Awake()
        {
            _sprites = GetComponentsInChildren<SpriteRenderer>();
        }
        
        public void SetLookDirection(Vector2 direction)
        {
            if (_initialDirectionRight)
            {
                if (direction.x > 0)
                {
                    FlipSprite(false);
                }
                else if (direction.x < 0)
                {
                    FlipSprite(true);
                }
            }
            else
            {
                if (direction.x > 0)
                {
                    FlipSprite(true);
                }
                else if (direction.x < 0)
                {
                    FlipSprite(false);
                }
            }
        }

        private void FlipSprite(bool flipX)
        {
            foreach (SpriteRenderer sprite in _sprites)
            {
                sprite.flipX = flipX;
            }
        }
    }
}