﻿using Global;
using UnityEngine;

namespace MenuSystem.Menus
{
	public class OptionsMenu : SimpleMenu<OptionsMenu>
	{
		public void OnVolumeChanged(float volume)
		{
			AudioController.Instance.MainMixer.SetFloat("volume", volume);
		}
	}
}
