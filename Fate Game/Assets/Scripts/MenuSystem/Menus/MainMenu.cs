﻿using MenuSystem.Menus;
using UnityEngine;

public class MainMenu : SimpleMenu<MainMenu>
{
	public void OnPlayPressed()
	{
		SceneLoader.Instance.LoadGameplayScene();
		GameMenu.Show();
	}

    public void OnOptionsPressed()
    {
		OptionsMenu.Show();
	}

	public override void OnBackPressed()
	{
		Application.Quit();
	}
}
