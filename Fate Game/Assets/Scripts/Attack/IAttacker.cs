﻿namespace Attack
{
    public interface IAttacker
    {
        public bool IsAttacking { get; }
        
        public void Attack(float duration, float distance);
        public void DealDamage();
    }
}