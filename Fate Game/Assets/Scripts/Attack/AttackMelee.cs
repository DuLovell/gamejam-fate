﻿using System.Collections;
using System.Runtime.CompilerServices;
using Movement;
using UnityEngine;

namespace Attack
{
    public class AttackMelee : MonoBehaviour, IAttacker, ICooldownable
    {
        private float _distance;
        
        public float Cooldown { get; set; }
        public bool IsOnCooldown { get; private set; }
        public bool IsAttacking { get; private set; }
        
        public void Attack(float duration, float distance)
        {
            _distance = distance;
            StartCoroutine(AttackRoutine(duration));
        }

        // Animation Event
        //TODO Учитывать направление движения персонажа
        public void DealDamage()
        {
            Debug.Log("Deal damage");
            float radius = _distance / 2f;
            Vector2 centerLocalPosition = new Vector2(radius, 0f);
            Collider2D[] enemiesHit = Physics2D.OverlapCircleAll((Vector2) transform.position + centerLocalPosition, radius);
            
            Debug.Log($"Enemies hit: {enemiesHit.Length}");
        }

        private IEnumerator AttackRoutine(float duration)
        {
            IsAttacking = true;
            yield return new WaitForSeconds(duration);
            IsAttacking = false;
            
            StartCoroutine(CooldownRoutine());
        }
        
        private IEnumerator CooldownRoutine()
        {
            IsOnCooldown = true;
            yield return new WaitForSeconds(Cooldown);
            IsOnCooldown = false;
        }
    }
}