﻿using loophouse.ScriptableStates;
using UnityEngine;

namespace FSM.Actions
{
    public abstract class ScriptableLastingAction : ScriptableAction
    {
        [SerializeField] private float _duration;
        
        public float Duration => _duration;
    }
}