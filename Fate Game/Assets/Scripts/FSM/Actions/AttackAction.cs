﻿using Attack;
using loophouse.ScriptableStates;
using Movement;
using UnityEngine;

namespace FSM.Actions
{
    [CreateAssetMenu(menuName = "Scriptable State Machine/Lasting Actions/Attack")]
    public class AttackAction : ScriptableLastingAction
    {
        [SerializeField] private float _distance;
        [SerializeField] private float _cooldown;

        public override void Act(StateComponent statesComponent)
        {
            IAttacker attacker = statesComponent.GetComponent<IAttacker>();
            ((ICooldownable) attacker).Cooldown = _cooldown;
            attacker.Attack(Duration, _distance);
        }
    }
}