﻿using loophouse.ScriptableStates;
using Movement;
using UnityEngine;

namespace FSM.Actions
{
    [CreateAssetMenu(menuName = "Scriptable State Machine/Lasting Actions/Dash")]
    public class DashAction : ScriptableLastingAction
    {
        [SerializeField] private float _distance;
        [SerializeField] private float _cooldown;

        public override void Act(StateComponent statesComponent)
        {
            IDashable dashable = statesComponent.GetComponent<IDashable>();
            ((ICooldownable) dashable).Cooldown = _cooldown;
            dashable.Dash(Duration, _distance);
        }
    }
}