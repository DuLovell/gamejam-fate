﻿using loophouse.ScriptableStates;
using Movement;
using Sirenix.OdinInspector;
using UnityEngine;
using Utils;

namespace FSM.Actions
{
    [CreateAssetMenu(menuName = "Scriptable State Machine/Actions/Play Animation")]
    public class PlayAnimationAction : ScriptableAction
    {
        [SerializeField] private string _stateName;
        [SerializeField] private AnimationClip _clip;
        [SerializeField] private bool _isLasting;
        [SerializeField] [ShowIf("_isLasting")] private ScriptableLastingAction _lastingAction;

        public override void Act(StateComponent statesComponent)
        {
            Animator animator = statesComponent.GetComponent<Animator>();
            animator.Play(_stateName);

            AdjustAnimationSpeed(animator);
        }

        private void AdjustAnimationSpeed(Animator animator)
        {
            if (_isLasting)
            {
                AnimationSpeedRegulator.SetCurrentAnimationSpeed(animator, _clip.length,_lastingAction.Duration);
            }
            else
            {
                AnimationSpeedRegulator.SetCurrentAnimationSpeed(animator, _clip.length);
            }
        }
    }
}