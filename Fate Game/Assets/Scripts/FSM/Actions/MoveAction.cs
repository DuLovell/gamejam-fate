﻿using loophouse.ScriptableStates;
using Movement;
using UnityEngine;

namespace FSM.Actions
{
    [CreateAssetMenu(menuName = "Scriptable State Machine/Actions/Move")]
    public class MoveAction : ScriptableAction
    {
        [SerializeField] private float _speed;
        
        public override void Act(StateComponent statesComponent)
        {
            statesComponent.GetComponent<IMovable>()?.Move(_speed);
        }
    }
}