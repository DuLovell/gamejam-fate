﻿using loophouse.ScriptableStates;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

namespace FSM.Conditions
{
    [CreateAssetMenu(menuName = "Scriptable State Machine/Conditions/Input", order = 1)]
    public class InputCondition : ScriptableCondition
    {
        [SerializeField] private InputActionAsset inputActionAsset;
        [SerializeField] private string actionName;
        
        private InputAction _inputAction;
        private ButtonControl _button;

        private void OnEnable()
        {
            _inputAction = inputActionAsset.FindAction(actionName);
        }

        public override bool Verify(StateComponent statesComponent)
        {
            _button = _inputAction.activeControl as ButtonControl;
            return _button != null && _inputAction.phase == InputActionPhase.Started;
        }
    }
}