﻿using loophouse.ScriptableStates;
using Movement;
using UnityEngine;

namespace FSM.Conditions
{
    [CreateAssetMenu(menuName = "Scriptable State Machine/Conditions/IsNotOnCooldown Dashing")]
    public class IsNotOnCooldownDashingCondition : ScriptableCondition
    {
        public override bool Verify(StateComponent statesComponent)
        {
            return !((ICooldownable) statesComponent.GetComponent<IDashable>()).IsOnCooldown;
        }
    }
}