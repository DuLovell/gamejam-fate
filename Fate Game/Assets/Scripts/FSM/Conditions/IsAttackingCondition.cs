﻿using Attack;
using loophouse.ScriptableStates;
using UnityEngine;

namespace FSM.Conditions
{
    [CreateAssetMenu(menuName = "Scriptable State Machine/Conditions/IsAttacking")]
    public class IsAttackingCondition : ScriptableCondition
    {
        public override bool Verify(StateComponent statesComponent)
        {
            return statesComponent.GetComponent<IAttacker>().IsAttacking;
        }
    }
}