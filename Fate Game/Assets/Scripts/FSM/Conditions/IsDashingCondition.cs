﻿using loophouse.ScriptableStates;
using Movement;
using UnityEngine;

namespace FSM.Conditions
{
    [CreateAssetMenu(menuName = "Scriptable State Machine/Conditions/IsDashing")]
    public class IsDashingCondition : ScriptableCondition
    {
        public override bool Verify(StateComponent statesComponent)
        {
            return statesComponent.GetComponent<IDashable>().IsDashing;
        }
    }
}