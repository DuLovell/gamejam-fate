﻿using Attack;
using loophouse.ScriptableStates;
using Movement;
using UnityEngine;

namespace FSM.Conditions
{
    [CreateAssetMenu(menuName = "Scriptable State Machine/Conditions/IsNotOnCooldown Attacking")]
    public class IsNotOnCooldownAttackingCondition : ScriptableCondition
    {
        public override bool Verify(StateComponent statesComponent)
        {
            return !(statesComponent.GetComponent<IAttacker>() as ICooldownable).IsOnCooldown;
        }
    }
}