using UnityEngine;

namespace Data.Variables
{
    [CreateAssetMenu(menuName = "Data/Variables/int")]
    public class IntVariable : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private int _initialValue;

        public int RuntimeValue { get; set; }

        public void OnAfterDeserialize()
        {
            RuntimeValue = _initialValue;
        }

        public void OnBeforeSerialize()
        {
        }
    }
}