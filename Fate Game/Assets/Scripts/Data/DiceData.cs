using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(menuName = "Data/Dice")]
    public class DiceData : ScriptableObject
    {
        [SerializeField] private List<Sprite> _sideSprites;

        public List<Sprite> SideSprites => _sideSprites;
    }
}
