using Data.Variables;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Data.Level
{
    [CreateAssetMenu(fileName = "Level N", menuName = "Data/Level", order = 0)]
    public class LevelData : ScriptableObject
    {
        [SerializeField] private IntVariable _dataToRandomize;
        [SerializeField] private EnemyToSpawn[] _enemies;
        [SerializeField] private Tilemap _environmentTilemap;

        public EnemyToSpawn[] Enemies => _enemies;
        public Tilemap EnvironmentTilemap => _environmentTilemap;
        public IntVariable DataToRandomize => _dataToRandomize;
    }
}