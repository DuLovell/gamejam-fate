using System;
using UnityEngine;

namespace Data.Level
{
    [Serializable]
    public struct EnemyToSpawn
    {
        public GameObject EnemyTemplate;
        public int Count;
    }
}