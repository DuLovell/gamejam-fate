﻿using System;
using System.Collections;
using Data;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace UI
{
    public class UIDiceRoller : MonoBehaviour
    {
        public event Action<int> DiceRolled;
    
        [SerializeField] private DiceData _data;
        [SerializeField] private Button _diceButton;

        private readonly WaitForSeconds _changeSideDelay = new WaitForSeconds(0.1f);

        private const int GUESS_ROUNDS_COUNT = 10;

        public void Roll()
        {
            StartCoroutine(RollRoutine());
        }

        private void OnEnable()
        {
            _diceButton.interactable = true;
        }

        private IEnumerator RollRoutine()
        {
            int randomDiceSideIndex = 0;
            int result = 0;
        
            for (int i = 0; i <= GUESS_ROUNDS_COUNT; i++)
            {
                randomDiceSideIndex = Random.Range(0, 6);
            
                _diceButton.image.sprite = _data.SideSprites[randomDiceSideIndex];
            
                yield return _changeSideDelay;
            }
        
            result = randomDiceSideIndex + 1;
        
            DiceRolled?.Invoke(result);
        }
    }
}
